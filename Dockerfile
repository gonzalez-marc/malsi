ARG PHP_VERSION=8.2.11
FROM php:${PHP_VERSION}-fpm-alpine

ARG ENABLE_COMPOSER=n
ARG USER_ID=1000

# hadolint ignore=SC3010
RUN apk update --no-cache \
  && apk add --no-cache \
    shadow~=4.13 \
  && usermod -u ${USER_ID} www-data \
  && groupmod -g ${USER_ID} www-data; \
  if [[ "${ENABLE_COMPOSER}" = "y" ]] ; then \
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php -r "if (hash_file('sha384', 'composer-setup.php') === 'e21205b207c3ff031906575712edab6f13eb0b361f2085f1f1237b7126d785e826a450292b6cfd1d64d92e6563bbde02') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
    && php composer-setup.php \
    && php -r "unlink('composer-setup.php');" \
    && mv composer.phar /usr/local/bin/composer ; \
  fi

USER www-data

WORKDIR /var/www/html

COPY . .

CMD ["php-fpm"]
